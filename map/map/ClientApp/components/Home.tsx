﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { } from '@types/markerclustererplus';
import { } from '@types/googlemaps';
import { } from '@types/google-maps';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import MarkerWithLabel from "react-google-maps/lib/components/addons/MarkerWithLabel"


const MyMapComponent = withScriptjs(withGoogleMap((props: any) =>
    <GoogleMap defaultZoom={8}  defaultCenter={{ lat: props.lat, lng: props.lng }} >

        {props.points.map((value) => (
            <MarkerWithLabel imagePath="https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m2.png" position={{ lat: value.latP, lng: value.lngP }} labelAnchor={new google.maps.Point(0, 0)} labelStyle={{ backgroundColor: "green", fontSize: "12px", padding: "8px" }} >
                <div>suda idi!</div>
            </MarkerWithLabel>
        ))}

        <MarkerWithLabel imagePath="https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m2.png" position={{ lat: props.lat2 , lng: props.lng2}} labelAnchor={new google.maps.Point(0, 0)} labelStyle={{ backgroundColor: "yellow", fontSize: "18px", padding: "10px" }} >
                <div>suda idi!(BONUS)</div>
            </MarkerWithLabel>
            
        <Marker position={{ lat: props.lat, lng: props.lng }} />

    </GoogleMap>
))

interface MapState {
    lat: number;
    lng: number;
    lat2: number;
    lng2: number;
    count: number;
    points: Array<IPoint>;
}

 interface IPoint {
     latP: number;
     lngP: number;
}

export default class Home extends React.Component<RouteComponentProps<{}>, MapState > {
    constructor() {
        super();
        this.state = { lat: -34.397, lng: 150.644, lat2: -34, lng2: 150, count: 0, points: [{ latP: -34, lngP: 150}]};
    }
    
    componentDidMount() {

        if (this.state.points.length < 10) {
            var list = this.state.points;
            for (var i = 0; i < 20; i++) {
                list[i] = { latP: (Math.random() * (100 - (-100)) + (-100)), lngP: (Math.random() * (200 - (-200)) + (-200)) };
            }

            this.setState({ points: list });
        }

       var z = this;
        document.addEventListener("keydown", keyDownHandler, false);

        function keyDownHandler(e: any) {
            if (e.keyCode == 87) {
                z.endPointList();
                z.endPoint();
                z.setState({ lat: z.state.lat + 0.06 });

            }
            if (e.keyCode == 83) {
                z.endPointList();
                z.endPoint();
                z.setState({ lat: z.state.lat - 0.06 });
            }
            if (e.keyCode == 65) {
                z.endPointList();
                z.endPoint();
                z.setState({ lng: z.state.lng - 0.06 });

            }
            if (e.keyCode == 68) {
                z.endPointList();
                z.endPoint();
                z.setState({ lng: z.state.lng + 0.06 });

            }
        }
    }

    endPoint() {
        if (Math.abs((this.state.lng - this.state.lng2)) < 0.05 && Math.abs((this.state.lat - this.state.lat2)) < 0.07) {
            this.setState({ lat2: this.state.lat2 + (Math.random() * (50 - (-50)) + (-50)), lng2: this.state.lng2 + (Math.random() * (50 - (-50)) + (-550)), count: this.state.count+5 });
        }
    }

    endPointList() {
        for (var i = 0; i < this.state.points.length; i++) {
            if (Math.abs((this.state.lng - this.state.points[i].lngP)) < 0.08 && Math.abs((this.state.lat - this.state.points[i].latP)) < 0.08) {
                var list = this.state.points;
                for (var i = 0; i < 20; i++) {
                    list[i] = { latP: (Math.random() * (100 - (-100)) + (-100)), lngP: (Math.random() * (100 - (-100)) + (-100)) };
                }

                this.setState({ points: list, count: this.state.count + 1 });
                break;
            }
        }
    }

    render() { 

        return <div style={{ height: "100%", width:"100%" }}>
            <MyMapComponent
                lng={this.state.lng}
                lat={this.state.lat}
                lng2={this.state.lng2}
                lat2={this.state.lat2}
                points={this.state.points}
                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: "100%", width: "100%"  }} />}
                containerElement={<div style={{ height:"950px", width:"100%" }} />}
                mapElement={<div style={{ height: "100%", width: "100%" }} />}               
            />

            <div>СЧЕТ:{this.state.count}</div>

        </div>;
    }
}

